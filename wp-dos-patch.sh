#! /bin/bash
#https://baraktawily.blogspot.fr/2018/02/how-to-dos-29-of-world-wide-websites.html
#https://github.com/Quitten/WordPress


if [[ -f wp-login.php && -f wp-admin/load-scripts.php && -f wp-admin/includes/noop.php ]]
then
		cp wp-login.php wp-login.php.ORG
		cp wp-admin/load-scripts.php wp-admin/load-scripts.php.ORG
		cp wp-admin/load-styles.php wp-admin/load-styles.php.ORG
		cp wp-admin/includes/noop.php wp-admin/includes/noop.php.ORG
		
		rm readme.html

        sed -i "1 s/^.*$/<?php\ndefine('CONCATENATE_SCRIPTS', false);/" wp-login.php
        sed -i -e "s/^require( ABSPATH . WPINC . '\/script-loader.php' );$/require( ABSPATH . 'wp-admin\/admin.php' );/g" wp-admin/load-scripts.php
        sed -i -e "s/^require( ABSPATH . WPINC . '\/script-loader.php' );$/require( ABSPATH . 'wp-admin\/admin.php' );/g" wp-admin/load-styles.php
        echo """<?php
/**
* Noop functions for load-scripts.php and load-styles.php.
*
* @package WordPress
* @subpackage Administration
* @since 4.4.0
*/

function get_file( \$path ) {
        if ( function_exists('realpath') ) {
                \$path = realpath( \$path );
        }
        if ( ! \$path || ! @is_file( \$path ) ) {
                return '';
        }
        return @file_get_contents( \$path );    
}""" > wp-admin/includes/noop.php
		echo 'Successfuly patched.'
else
        echo 'Please run this file from WordPress root directory.'
fi
