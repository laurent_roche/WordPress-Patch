<?php
/***
 * Get and Run the script to patch against dos: wp-dos-patch.sh
 * Copy the file at your root directory and run:
     https://my.site.com/wp-dos-patch.php
*/
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8">
<title>WP DOS Patch
</title>
</head>
<body>
<h1>WP DOS Patch</h1>
<pre>
<?php

	if (system('wget https://framagit.org/laurent_roche/WordPress-Patch/raw/master/wp-dos-patch.sh', $returnCode)) {
		echo "</pre><p>Command <em>wget</em>: OK !  (code={$returnCode})</p>";
	} else {
		if ( $returnCode == 0) {
			echo "</pre><p>Command <em>wget</em>: OK !  (code={$returnCode})</p>";
		} else {
			echo "</pre><p> Command <em>wget</em> failed with code={$returnCode}.</p>";
		}
	}
	if (system('chmod u+x wp-dos-patch.sh', $returnCode)) {
			echo "</pre><p>Command <em>chmod</em> : OK !  (code={$returnCode})</p>";
	} else { 
		if ( $returnCode == 0) {
			echo "</pre><p>Command <em>chmod</em> : OK !  (code={$returnCode})</p>";
		} else {
			echo "</pre><p>Command <em>chmod</em> failed with code={$returnCode}.</p>";
		}
	}
	if (system('./wp-dos-patch.sh', $returnCode)) {
		echo "</pre><p>Script <em>wp-dos-patch.sh</em> : OK !  (code={$returnCode})</p>";
	} else { 
		if ( $returnCode == 0) {
			echo "</pre><p>Script <em>wp-dos-patch.sh</em> : OK !  (code={$returnCode})</p>";
		} else {
			echo "</pre><p>Script <em>wp-dos-patch.sh</em> failed with code={$returnCode}.</p>";
		}
	}
?> 
</body>

