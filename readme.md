## Description

This is a shell script to run in your *WordPress* directory to patch *WordPress* against a *DOS* attack as decribed on [Barak Tawily's blog](https://baraktawily.blogspot.fr/2018/02/how-to-dos-29-of-world-wide-websites.html).

Being a shell script, this only works on *Linux* servers, not on *Windows* servers (that should work on *MacOs X* servers - as I understand they do have a shell- but I am not sure how).

The difference with the [original script](https://github.com/Quitten/WordPress/blob/master/wp-dos-patch.sh) is :

 - copying the original files before modifying them
 - removing `readme.html`
 
## Usage

### with ssh access

Jut get the file by running :

```shell
$ wget https://framagit.org/laurent_roche/WordPress-Patch/raw/master/wp-dos-patch.sh
```

then run the file `wp-dos-patch.sh` !

That's it !

### no ssh access: PHP script

Alternatively (if you don't have *ssh* access), you will copy / ftp the file `wp-dos-patch.php` at your *WordPress* root directory and access:  
      https://my.site.com/wp-dos-patch.php  
(that will get the latest version of the shell script and run it.)

### WordPress updates

Keep in mind, you will have to **run the patch _after each_ WordPress update** ! :wink:

